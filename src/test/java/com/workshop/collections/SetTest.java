package com.workshop.collections;

import org.junit.Test;
import java.util.Set;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class SetTest {
    @Test
    public void shouldBeSortedItems() {
/*        Set<Integer> items;
        items.add(3);
        items.add(5);
        items.add(1);

        assertThat(items, contains(1, 3, 5));*/

        fail("Initialize the above set and uncomment the code above so that items are in order");
    }

    @Test
    public void shouldPreserveOrder() {
      /*  Set<Integer> items;
        items.add(3);
        items.add(8);
        items.add(5);
        items.add(9);
        items.add(1);

        assertThat(items, contains(3, 8,5, 9, 1));*/

      fail("Initialize the above set and uncomment the code above so that items preserve their insertion order");
    }
}
