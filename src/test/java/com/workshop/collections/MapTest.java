package com.workshop.collections;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.hasKey;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class MapTest {

    @Test
    public void shouldFindTheStudent() {
        Map<Student, Integer> grades = new HashMap();
        grades.put(new Student("John", "Doe"), 10);
        grades.put(new Student("John", "Snow"), 9);

        assertThat(grades, hasKey(new Student("John", "Doe")));
        assertNotNull(grades.get(new Student("John", "Doe")));
    }
}
