package com.workshop.collections;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class ListTest {
    List <Student> students;

    @Before
    public void initialize() {
        students = new ArrayList<>();
        students.add(new Student("John", "Who"));
        students.add(new Student("John", "Doe"));
        students.add(new Student("John", "Snow"));
    }

    @Test(expected = java.util.ConcurrentModificationException.class)
    public void shouldRemoveItemFromList() {
        for(Student student: students) {
            if (student.getLastName().equals("Doe")) {
                students.remove(student);
            }
        }

        assertThat(students, not(hasItem(new Student("John", "Doe"))));
    }

    @Test
    public void shouldBeSorted() {
        /*Collections.sort(students);
        assertThat(students, contains(new Student("John", "Doe"), new Student("John", "Snow"), new Student("John", "Who")));
        */
        fail("Write code in Student class so that uncommenting the line above, test will pas");
    }

}
