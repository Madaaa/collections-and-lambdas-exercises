package com.workshop.lambdas;

import com.workshop.collections.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;

// TODO refactor each test to use lambda expressions
public class LambdaRefactorTest {
    List<Student> students;

    @Before
    public void initilaize() {
        students = new ArrayList<>();
        students.add(new Student("John", "Doe", 13));
        students.add(new Student("Anna", "Doe", 10));
        students.add(new Student("Barbare", "NotDoe", 20));
        students.add(new Student("Sansa", "Doe", 50));
    }

    @Test
    public void studentsShouldBeSortedByFirstName() {
        students.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getFirstName().compareTo(o2.getFirstName());
            }
        });

        assertThat(students, is(Arrays.asList(new Student("Anna", "Doe", 10), new Student("Barbare", "NotDoe", 20), new Student("John", "Doe", 13), new Student("Sansa", "Doe", 50))));
    }

    @Test
    public void studentsShouldBeOnlyDoes() {
        List<Student> filteredStudents = new ArrayList<>();

        for(Student student: students) {
            if(student.getLastName().equals("Doe")) {
                filteredStudents.add(student);
            }
        }

        assertThat(filteredStudents, containsInAnyOrder(new Student("John", "Doe", 13), new Student("Anna", "Doe", 10), new Student("Sansa", "Doe", 50)));
    }

    @Test
    public void shouldGetTheMaxmimumAge() {
        int max = 0;

        for(Student student: students) {
            if (student.getAge() > max) {
                max = student.getAge();
            }
        }

        assertThat(max, is(50));
    }
}
