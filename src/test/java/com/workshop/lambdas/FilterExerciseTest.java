package com.workshop.lambdas;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class FilterExerciseTest {
    @Test
    public void shoudFilterOddNumbers() {
        Stream<Integer> numbers = Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        List<Integer> evenNumbers = new ArrayList<>();

        assertThat(evenNumbers, is(Arrays.asList(1, 3, 5, 7, 9)));
    }
}
