package com.workshop.lambdas;

import org.junit.Test;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class ReduceExerciseTest {

    @Test
    public void shouldComputeTheSumOfNumbers() {
        Stream<Integer> numbers = Stream.of(1, 3, 5, 7, 9);

        int sum = 0;

        assertThat(sum, is(25));
    }
}
