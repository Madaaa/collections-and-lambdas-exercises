package com.workshop.lambdas;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MapExerciseTest {

    @Test
    public void shouldIncreaseNumbersByOne() {
        Stream<Integer> numbers = Stream.of(0, 1, 2, 3, 4);

        List<Integer> doubleNumbers = new ArrayList<>();

        assertThat(doubleNumbers, is(Arrays.asList(1, 2, 3, 4, 5)));
    }

}
